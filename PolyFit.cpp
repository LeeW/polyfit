#include <iostream>

using namespace std;

void PolyFit(const double* x, double* y, const size_t n, double* outputCoefficients, bool bFixedStart)
{
	// Quadratic polynomial regression for n pairs of x,y coordinates
	// This calculates an estimate of the best polynomial coefficients for the given data

	// The sum of squares matrix is generated from a 3*n matrix from the input x data
	// at pow0, pow1, and pow2 for each row respectively. That matrix would then be multiplied
	// with a transposed copy of itself. But it is trivial to just jump to calculating the end result
	// which is a 3x3 matrix as follows:
	// [x^0][x^1][x^2]
	// [x^1][x^2][x^3]
	// [x^2][x^3][x^4]
	// There are only 5 unique values in the matrix so this has been further reduced, but the matrix
	// based code is left here for reference:
	//double sumSqr[3][3] = { {double(n), 0, 0}, {0, 0, 0}, {0, 0, 0} };
	//for (int i = 0; i < n; i++) {
	//	sumSqr[0][1] += x[i];// sum of x pow 1
	//	sumSqr[1][1] += x[i]*x[i];// sum of x pow 2
	//	sumSqr[1][2] += x[i]*x[i]*x[i];// sum of x pow 3
	//	sumSqr[2][2] += x[i]*x[i]*x[i]*x[i];// sum of x pow 4
	//}
	//// Symmetry along the diagonal
	//sumSqr[1][0] = sumSqr[0][1];
	//sumSqr[0][2] = sumSqr[1][1];
	//sumSqr[2][0] = sumSqr[1][1];
	//sumSqr[2][1] = sumSqr[1][2];
	//// determinant of the matrix
	//double deter = sumSqr[0][0] * (sumSqr[1][1] * sumSqr[2][2] - sumSqr[1][2] * sumSqr[2][1])
	//             - sumSqr[0][1] * (sumSqr[1][0] * sumSqr[2][2] - sumSqr[1][2] * sumSqr[2][0])
	//             + sumSqr[0][2] * (sumSqr[1][0] * sumSqr[2][1] - sumSqr[1][1] * sumSqr[2][0]);
	//// Inverse sum of squares matrix
	//// (cofactor / determinant) to get the inverse of the matrix
	//double invSumSqr[3][3];
	//invSumSqr[0][0] =  ((sumSqr[1][1] * sumSqr[2][2]) - (sumSqr[1][2] * sumSqr[2][1])) / deter;
	//invSumSqr[0][1] = -((sumSqr[1][0] * sumSqr[2][2]) - (sumSqr[1][2] * sumSqr[2][0])) / deter;
	//invSumSqr[0][2] =  ((sumSqr[1][0] * sumSqr[2][1]) - (sumSqr[1][1] * sumSqr[2][0])) / deter;
	//invSumSqr[1][0] = -((sumSqr[0][1] * sumSqr[2][2]) - (sumSqr[0][2] * sumSqr[2][1])) / deter;
	//invSumSqr[1][1] =  ((sumSqr[0][0] * sumSqr[2][2]) - (sumSqr[0][2] * sumSqr[2][0])) / deter;
	//invSumSqr[1][2] = -((sumSqr[0][0] * sumSqr[2][1]) - (sumSqr[0][1] * sumSqr[2][0])) / deter;
	//invSumSqr[2][0] =  ((sumSqr[0][1] * sumSqr[1][2]) - (sumSqr[0][2] * sumSqr[1][1])) / deter;
	//invSumSqr[2][1] = -((sumSqr[0][0] * sumSqr[1][2]) - (sumSqr[0][2] * sumSqr[1][0])) / deter;
	//invSumSqr[2][2] =  ((sumSqr[0][0] * sumSqr[1][1]) - (sumSqr[0][1] * sumSqr[1][0])) / deter;


	double sumXPow0 = n;// x pow 0 = 1, therefore sum for n points = n
	double sumXPow1 = 0.0, sumXPow2 = 0.0, sumXPow3 = 0.0, sumXPow4 = 0.0;
	for (int i = 1; i < n; i++)
	{
		double xi = x[i]-x[0];
		double dX = xi;
		sumXPow1 += dX;
		dX *= xi;
		sumXPow2 += dX;
		dX *= xi;
		sumXPow3 += dX;
		dX *= xi;
		sumXPow4 += dX;
	}
	// determinant of the matrix
	double deter = (sumXPow0 * ((sumXPow2 * sumXPow4) - (sumXPow3 * sumXPow3)))
	             - (sumXPow1 * ((sumXPow1 * sumXPow4) - (sumXPow3 * sumXPow2)))
	             + (sumXPow2 * ((sumXPow1 * sumXPow3) - (sumXPow2 * sumXPow2)));
	// Inverse sum of squares matrix
	// (cofactor / determinant) to get the inverse of the matrix
	double invSumSqr[3][3];
	invSumSqr[0][0] =  ((sumXPow2 * sumXPow4) - (sumXPow3 * sumXPow3)) / deter;
	invSumSqr[0][1] = -((sumXPow1 * sumXPow4) - (sumXPow3 * sumXPow2)) / deter;
	invSumSqr[0][2] =  ((sumXPow1 * sumXPow3) - (sumXPow2 * sumXPow2)) / deter;
	invSumSqr[1][0] = -((sumXPow1 * sumXPow4) - (sumXPow2 * sumXPow3)) / deter;
	invSumSqr[1][1] =  ((sumXPow0 * sumXPow4) - (sumXPow2 * sumXPow2)) / deter;
	invSumSqr[1][2] = -((sumXPow0 * sumXPow3) - (sumXPow1 * sumXPow2)) / deter;
	invSumSqr[2][0] =  ((sumXPow1 * sumXPow3) - (sumXPow2 * sumXPow2)) / deter;
	invSumSqr[2][1] = -((sumXPow0 * sumXPow3) - (sumXPow2 * sumXPow1)) / deter;
	invSumSqr[2][2] =  ((sumXPow0 * sumXPow2) - (sumXPow1 * sumXPow1)) / deter;
	// Sum of Y multiplied by sum of squares for associated X
	double XY[3] = { 0.0, 0.0, 0.0};
	for (int i = 1; i < n; i++)
	{
		double xi = x[i]-x[0];
		XY[0] += (y[i]-y[0]);// sum(yi * xi pow 0)
		XY[1] += (y[i]-y[0]) * xi;// sum(yi * xi pow 1)
		XY[2] += (y[i]-y[0]) * (xi * xi);// sum(yi * xi pow 2)
	}
	// Coefficients estimated by multiplying the inverse sum of squares matrix by the XY matrix
	outputCoefficients[0] = y[0];
	for (int i = 0; i < 3; i++) {
		if (!bFixedStart)
			outputCoefficients[0] += invSumSqr[0][i] * XY[i];
		outputCoefficients[1] += invSumSqr[1][i] * XY[i];
		outputCoefficients[2] += invSumSqr[2][i] * XY[i];
	}
}

void PolyFitOptimized(const double* x, double* y, const size_t n, double* coefficients)
{
	// Polynomial Regression
	// This is an optimized version of the PolyFit function, with a fixed intercept the sum of squares matrix
	// that we would generate would be zero'd out for the top row and left column which negates a lot of the work
	// SumOfSquaresMatrix[0][0] is 1 but the rest of the row and column is 0
	double sumXPow2 = 0.0, sumXPow3 = 0.0, sumXPow4 = 0.0;
	for (int i = 1; i < n; i++)
	{
		double xi = x[i] - x[0];
		double dX = xi * xi;
		sumXPow2 += dX;
		dX *= xi;
		sumXPow3 += dX;
		dX *= xi;
		sumXPow4 += dX;
	}
	// determinant of the matrix
	double deter = (sumXPow2 * sumXPow4) - (sumXPow3 * sumXPow3);
	// Sum of Y multiplied by sum of squares for associated X
	double XY[2] = { 0.0, 0.0 };
	for (int i = 1; i < n; i++)
	{
		double xi = x[i] - x[0];
		XY[0] += (y[i]-y[0]) * xi;
		XY[1] += (y[i]-y[0]) * (xi * xi);
	}
	// Coefficients
	coefficients[0]  = y[0];
	coefficients[1]  = ( sumXPow4 / deter) * XY[0];
	coefficients[1] += (-sumXPow3 / deter) * XY[1];
	coefficients[2]  = (-sumXPow3 / deter) * XY[0];
	coefficients[2] += ( sumXPow2 / deter) * XY[1];
}

double GetYFromXAndQuadraticCoefficients(const double x, const double* coeff)
{
	return coeff[0] + coeff[1]*x + coeff[2]*x*x;
}


int main(int argc, char* argv[])
{
	constexpr double x[] = { -10.0, -9.2, -8.6, -7.420, -6.6, -6.1};
	constexpr double y[] = { 15.234, 16.6, 17.5, 17.2, 15.6, 15.4 };
	constexpr size_t n = sizeof(x) / sizeof(double);
	static_assert((n == (sizeof(y) / sizeof(double))));

	// Generate y values of a quadratic curve from given coefficients to test exact match
//	double GC[3] = {2.0, -1.0, 10.0};
//	for (unsigned int i = 0; i < 6; i++) {
//		y[i] = GC[2]*x[i]*x[i] + GC[1]*x[i] + GC[0];
//	}

	double coefficients[3] = {0.0, 0.0, 0.0};

	// Polyfit, start point can be modified
	cout << "PolyFit" << endl;
	PolyFit((double*)x, (double*)y, n, coefficients, false);
	cout << "coeff:" << coefficients[0] << " " << coefficients[1] << " " << coefficients[2] << endl;
	for (int i = 0; i < n; i++)
	{
		cout << x[i];
		cout << ",";
		cout << y[i];
		cout << ",";
		cout << GetYFromXAndQuadraticCoefficients(x[i]-x[0], coefficients);
		cout << endl;
	}
	cout << endl;

	// Polyfit, start point fixed
	cout << "PolyFit, Fixed Start" << endl;
	coefficients[0] = coefficients[1] = coefficients[2] = 0.0;
	PolyFit((double*)x, (double*)y, n, coefficients, true);
	cout << "coeff:" << coefficients[0] << " " << coefficients[1] << " " << coefficients[2] << endl;
	for (int i = 0; i < n; i++)
	{
		cout << x[i];
		cout << ",";
		cout << y[i];
		cout << ",";
		cout << GetYFromXAndQuadraticCoefficients(x[i]-x[0], coefficients);
		cout << endl;
	}
	cout << endl;

	// Polyfit start point fixed, optimized
	cout << "PolyFitOptimized (Fixed Start)" << endl;
	coefficients[0] = coefficients[1] = coefficients[2] = 0.0;
	PolyFitOptimized((double*)x, (double*)y, n, coefficients);
	cout << "coeff:" << coefficients[0] << " " << coefficients[1] << " " << coefficients[2] << endl;
	for (int i = 0; i < n; i++)
	{
		cout << x[i];
		cout << ",";
		cout << y[i];
		cout << ",";
		cout << GetYFromXAndQuadraticCoefficients(x[i]-x[0], coefficients);
		cout << endl;
	}
	cout << endl;

	getchar();// pause for input instead of return
}